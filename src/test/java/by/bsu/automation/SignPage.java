package by.bsu.automation;

import by.bsu.automation.utils.ConfigurationValues;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.sql.Driver;

public class SignPage {

    private final By inputLink = By.xpath("//a[text()='Войти']");

    private final By inputLogin= By.xpath("//input[@name='login']");

    private final By inputPassword = By.xpath("//input[@name='password']");

    private final By inputButton = By.xpath("//input[@value='Войти']");

    public WebDriver signIn(){
        WebDriver driver = new FirefoxDriver();
        driver.get(ConfigurationValues.TUT_BY);
        driver.findElement(inputLink).click();
        driver.findElement(inputLogin).sendKeys(ConfigurationValues.TUT_BY_USERNAME);
        driver.findElement(inputPassword).sendKeys(ConfigurationValues.TUT_BY_PASSWORD);
        driver.findElement(inputButton).click();
        return driver;
    }
}
