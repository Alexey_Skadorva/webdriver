package by.bsu.automation.tests;
import by.bsu.automation.SignPage;
import by.bsu.automation.utils.ConfigurationValues;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class MailTest {

    private static Logger logger = Logger.getLogger(MailTest.class);

    private final By buttonSignInLocator = By.xpath("//a[text()='Почта']");

    @Test
    public void testOneCanLoginGithub(){
        WebDriver driver = new FirefoxDriver();
        driver.get(ConfigurationValues.TUT_BY);
        driver.findElement(By.xpath("//a[text()='Войти']")).click();
        driver.findElement(By.xpath("//input[@name='login']")).sendKeys(ConfigurationValues.TUT_BY_USERNAME);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(ConfigurationValues.TUT_BY_PASSWORD);
        driver.findElement(By.xpath("//input[@value='Войти']")).click();
        System.out.println("Enter");
        driver.findElement(buttonSignInLocator).click();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//a[@title='Написать (w, c)']")).click();
        System.out.println("Sending");
        driver.findElement(By.className("b-mail-input_yabbles__focus")).sendKeys(ConfigurationValues.GOOGLE_COM_USERNAME);
        driver.findElement(By.xpath("//input[@name='subj']")).sendKeys("Automation testing");
        driver.findElement(By.xpath("//button[@id='compose-submit']")).submit();
        driver.quit();

        WebDriver driverr = new FirefoxDriver();

        driverr.get("http://mail.google.com/");
        driverr.findElement(By.xpath("//input[@id='Email']")).sendKeys(ConfigurationValues.GOOGLE_COM_USERNAME);
        driverr.findElement(By.xpath("//input[@id='next']")).click();
        driverr.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driverr.findElement(By.xpath("//input[@name='Passwd']")).sendKeys(ConfigurationValues.GOOGLE_COM_PASSWORD);
        driverr.findElement(By.xpath("//input[@id='signIn']")).click();
        driverr.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driverr.findElement(By.className("yW")).click();
        String incomeMessage = driverr.findElement(By.className("hP")).getText();

        Assert.assertEquals(incomeMessage, "Automation testing");
        driverr.quit();
    }
}
