package by.bsu.automation.tests;

import by.bsu.automation.utils.ConfigurationValues;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey Skadorva on 20.12.2015.
 */
public class SearchTest {

    private static Logger logger = Logger.getLogger(SearchTest.class);

    private final By buttonSignInLocator = By.xpath("//a[text()=' — Википедия']");

    @Test
    public void testOneCanLoginTutBy(){
        WebDriver driver = new FirefoxDriver();
        driver.get(ConfigurationValues.TUT_BY);
        driver.findElement(By.xpath("//input[@id='search_from_str']")).sendKeys("Сирия");
        driver.findElement(By.xpath("//input[@name='search']")).click();
        driver.findElement(buttonSignInLocator).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Assert.assertEquals(1, 1);
        driver.quit();
    }
}
