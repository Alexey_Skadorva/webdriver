package by.bsu.automation.tests;

import by.bsu.automation.utils.ConfigurationValues;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey Skadorva on 27.12.2015.
 */
public class CategoryTest {

    private static Logger logger = Logger.getLogger(CategoryTest.class);

    @Test
    public void testOneCanLoginTutBy(){
        WebDriver driver = new FirefoxDriver();
       driver.get(ConfigurationValues.TUT_BY);
        driver.findElement(By.xpath("//a[text()='Войти']")).click();
        driver.findElement(By.xpath("//input[@name='login']")).sendKeys(ConfigurationValues.TUT_BY_USERNAME);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(ConfigurationValues.TUT_BY_PASSWORD);
        driver.findElement(By.xpath("//input[@value='Войти']")).click();
        driver.findElement(By.className("entry-pic")).click();
        Actions action = new Actions(driver);
        action.click(driver.findElement(By.xpath("//a[text()='Перейти к обсуждению статьи на форуме']")));
        String likes= driver.findElement(By.className("count")).getText();

        driver.findElement(By.className("b-icon icon-handUp")).click();

        String newLikes = driver.findElement(By.className("count")).getText();
        Assert.assertTrue(Integer.valueOf(likes)+1 == Integer.valueOf(newLikes));

        driver.quit();
    }

    public void scrollAndClick(WebDriver driver,By by)
    {
        WebElement element = driver.findElement(by);
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();"
                ,element);
        element.click();
    }
}
