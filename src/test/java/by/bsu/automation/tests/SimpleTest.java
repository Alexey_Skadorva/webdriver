package by.bsu.automation.tests;

import by.bsu.automation.SignPage;
import by.bsu.automation.utils.ConfigurationValues;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Alexey Skadorva on 20.12.2015.
 */
public class SimpleTest {

    private static Logger logger = Logger.getLogger(SimpleTest.class);

    private final By userNameInSystem = By.className("uname");
    SignPage signPage = new SignPage();

    @Test
        public void testOneCanLoginTutBy(){
            WebDriver driver = signPage.signIn();
            String userName = driver.findElement(userNameInSystem).getText();
            Assert.assertEquals(userName, "Алексей Скадорва");
            driver.quit();
        }
}
