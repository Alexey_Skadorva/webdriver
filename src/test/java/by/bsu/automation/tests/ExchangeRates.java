package by.bsu.automation.tests;

import by.bsu.automation.utils.ConfigurationValues;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey Skadorva on 27.12.2015.
 */
public class ExchangeRates {

    private static Logger logger = Logger.getLogger(ExchangeRates.class);

    @Test
    public void testOneCanLoginTutBy(){
        WebDriver driver = new FirefoxDriver();
        driver.get(ConfigurationValues.TUT_BY);
        driver.findElement(By.xpath("//a[text()='Финансы']")).click();
        driver.findElement(By.xpath("//a[text()='Курсы валют']")).click();
        driver.findElement(By.xpath("//a[text()='Доллар США']")).click();
        driver.findElement(By.xpath("//a[text()='Евро']")).click();
        String euro = driver.findElement(By.className("value")).getText();
        Assert.assertEquals(euro, "1.09");
        driver.quit();
    }
}
